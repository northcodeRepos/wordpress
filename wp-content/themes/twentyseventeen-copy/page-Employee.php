<?php
/**
 * The template for displaying the contact
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<?php

get_header(); ?>

<div class="wrap">
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
            while (have_posts()) : the_post();

                get_template_part('template-parts/page/content', 'page');

                // If comments are open or we have at least one comment, load up the comment template.
                if (comments_open() || get_comments_number()) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->


<div class="wrap">

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">


            <article id="post-10" class="post-10 page type-page status-publish has-post-thumbnail hentry">

                <div class="entry-content full-width">
                    <h2><?php the_field('name'); ?></h2>
                    <h6><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></h6>
                    <h6><a href="<?php the_field('www'); ?>" target="_blank"><?php the_field('www'); ?></a></h6>
                    <h6><a href="<?php the_field('facebook'); ?>" target="_blank"><?php the_field('facebook'); ?></a>
                    </h6>
                    <h6><a href="<?php the_field('twitter'); ?>" target="_blank"><?php the_field('twitter'); ?></a></h6>
                    <h6><a href="<?php the_field('linkedin'); ?>" target="_blank"><?php the_field('linkedin'); ?></a>
                    </h6>
                    <br>
                    <hr>
                    <h2>Related Posts :</h2>
                    <?php
                    $posts = get_field('related_posts');

                    if ($posts): ?>
                        <ul class="rel_posts">
                            <?php foreach ($posts as $p): // variable must NOT be called $post (IMPORTANT) ?>
                                <li class="rel_post">

                                    <img src="<?php echo get_the_post_thumbnail_url($p->ID); ?>" alt="Post thumbnail" />
                                    <div>
                                        <h2><?php echo get_the_title($p->ID); ?></h2>
                                        <p><?php echo substr($p->post_content, 0, 50) ?>... <br><small><a href="<?php echo get_permalink($p->ID); ?>">Read More -></a></small></p>

                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div><!-- .entry-content -->
            </article><!-- #post-## -->

        </main><!-- #main -->
    </div>

</div>


<?php get_footer(); ?>


