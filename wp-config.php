<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P]3Q&tv98/_Dq9b[nI3/[)xvK7m;=et4AqNf+6mzQp$!P3O1a#2FE=~X0qg}PK(?');
define('SECURE_AUTH_KEY',  'QZ4$shqTA7EHGc/y5Sjd>SKCg0x}_pUBup?Nl$49/&(>jqPlayJl9odj4qJ#_v(k');
define('LOGGED_IN_KEY',    '_k2ogjFbWb7KNdh*oQ8E)bkt|T~]K|d2Tk#;hYX&*QY/PH:]Msus5cYt6##rod.A');
define('NONCE_KEY',        'b=bbez,_{iCq:cv7pnbK^!e`:EM&-tt%CBm}2 =Q*:6QCA&?jbpYM:TbVr*b=}v}');
define('AUTH_SALT',        '8A]^hKx]wYQaq%#|-C|>(9C&blc@<2z)Zh~qJwW{d~!6G%qd#o(tZh+a(d LV=P/');
define('SECURE_AUTH_SALT', 'ob,vVF|9[$qz|cU^k7B`F.gXMmeu.)Y>|Uv?& 7oIrQMjpTq(pPl2W4Pu(%o8#ZN');
define('LOGGED_IN_SALT',   '~EY(VoOuUQei4QK->{eEIA9/>q+*MhB)0]IN],i7p</SI%b35/gL$B=~FfI>cgBA');
define('NONCE_SALT',       'f$9=SVrU+m-A}.,czLId1lV<5zF5M<wswt+ EUbYy{>Y.UtH_(zqxdNt,o=0u^^?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
